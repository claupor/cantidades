'''
Maneja un diccionario con cantidades
'''

import sys

items = {}

def op_add():
    """Add an item and quantity from argv to the items dictionary"""
    item = sys.argv[0]
    quantity = int(sys.argv[1])
    adlist = {item: quantity}
    items.update(adlist)
    return items


def op_items():
    """Print all items, separated by spaces"""
    for i in items.keys():
        print(i, end=" ")


def op_all():
    """Print all items and quantities, separated by spaces"""
    for j in items.items():
        print(j)


def op_sum():
    """Print sum of all quantities"""
    sumlist = sum(items.values())
    print(sumlist)


def main():
    while sys.argv != 0:
        op = sys.argv.pop(0)
        if op == "add":
            op_add()
        elif op == "items":
            op_items()
        elif op == "sum":
            op_sum()
        elif op == "all":
            op_all()


if __name__ == '__main__':
    main()
